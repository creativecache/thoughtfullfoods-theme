<?php
/**
 * The front page template file.
 *
 * @package storefront
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
				$args = array(
					'post_type'   =>  'product',
					'stock'       =>  1,
					'showposts'   =>  6,
					'orderby'     =>  'date',
					'order'       =>  'DESC',
				);
			
				$loop = new WP_Query( $args );
				if ( $loop->have_posts() ) :
   					
			?>
			
			<section id="product-slider">
				<?php if ($loop->post_count > 1) : ?>
				<nav class="product-slider-nav">
					<ul>
						<li class="prev">
							<button type="button"><i class="fas fa-chevron-left"></i></button>
						</li>
						<li class="next">
							<button type="button"><i class="fas fa-chevron-right"></i></button>
						</li>
					</ul>
				</nav>
				
				<ul>
				<?php
					endif;
					while ( $loop->have_posts() ) : $loop->the_post();
				?>
					<?php
						global $product;
					
						if ( has_post_thumbnail( $product->post->ID ) ) :
					?>
					<li class="product-slider-item">
						<div class="product-image">
							<a href="<?php echo the_permalink(); ?>">
								<img src="<?php echo the_post_thumbnail_url(); ?>" alt="<?php echo bloginfo('name'); ?> <?php echo the_title(); ?>" />
							</a>
						</div>
						<div class="product-details">
							<div class="product-details-container">
								<h2><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h2>
								<?php if (has_excerpt()) : ?>
								<?php //echo the_excerpt(); ?>
								<?php endif; ?>
							</div>
						</div>
					</li>
					<?php endif; ?>
					<?php endwhile;?>
				</ul>
			</section>
			
			<?php endif; ?>
			
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
