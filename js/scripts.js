jQuery(document).ready(function($) {
	
	var numItems = $('#product-slider .product-slider-item').length,
		activeWidth = 40 / (numItems - 1),
		colors = ['#2a9d8f','#f4a261','#e76f51','#e9c369','#264653'];
	
	$('#product-slider .product-slider-item:first').addClass('first active');
	$('#product-slider .product-slider-item:first').next().addClass('next');
	$('#product-slider .product-slider-item:last').addClass('last');
	$('#product-slider .product-slider-nav .prev').hide();
	$('#product-slider .product-slider-item:not(.first)').css('width', activeWidth + '%');
	$('.product-slider-nav button').click(function() {
	
	$('#product-slider .product-slider-item a').click(function(e) {
		e.preventDefault();
		
		if ($(this).closest('.product-slider-item').hasClass('active')) {
			$(this).trigger('click');
		} else {
			$('#product-slider .product-slider-item.active').css('width', activeWidth + '%').removeClass('active');
			$(this).closest('.product-slider-item').css('width','').addClass('active');
		}
	});
	
});